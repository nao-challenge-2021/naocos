---
title: About Us
subtitle: NaoCos
comments: false
---

Noi siamo la squadra NaoCos. Noi siamo studenti apparteneneti alla terza liceo scientifico dell'istituto Rainerum. La nostra squadra è composta da 12 componenti che sono: Mattia, Philip, Patrick, Valerio, Gianmaria, Federico, Mathias, Eric, Aaron, Dominick, Lorenzo, Matteo.

We are the NaoCos team. We are students belonging to the third scientific high school of the Rainerum institute. Our team is made up of 12 members who are: Mattia, Philip, Patrick, Valerio, Gianmaria, Federico, Mathias, Eric, Aaron, Dominick, Lorenzo, Matteo.

Wir sind das NaoCos-Team. Wir sind Schüler der dritten wissenschaftlichen Gymnasium des Rainerum-Instituts. Unser Team besteht aus 12 Studenten: Mattia, Philip, Patrick, Valerio, Gianmaria, Federico, Mathias, Eric, Aaron, Dominick, Lorenzo und Matteo.

**IL NOSTRO NOME**

Il nostro nome, NaoCos, deriva dalla famosissima serie TV Narcos, da cui noi abbiamo preso spunto per il nostro nome.

Our name, NaoCos, derives from the very famous TV series Narcos, from which we took our name from.

Unser Name, NaoCos, leitet sich von der sehr berühmten TV-Serie Narcos ab, von der wir unseren Namen abgeleitet haben.
