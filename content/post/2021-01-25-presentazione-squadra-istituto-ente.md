---
title: Video di presentazione della squadra, dell'ente e dell'istituto
date: 2021-01-25
bigimg: [{src: "/img/triangle.jpg"}]
---

{{< youtube id="6-YgvUT8ZYY" >}}

Alcuni membri della nostra squadra hanno realizzato questo video per presentare il nostro gruppo, la nostra scuola e l'ente con cui lavoriamo.

Some members of our team have made this video to present our group, our school and the organization we work with.

Einige Mitglieder unseres Teams haben dieses Video gemacht, um unsere Gruppe, unsere Schule und die Organisation, mit der wir zusammenarbeiten, vorzustellen.
