---
title: Progetto della didattica interattiva
date: 2021-03-08
bigimg: [{src: "/img/triangle.jpg"}]
---

Il nostreo team ha deciso di portare un progetto sulla didattica interattiva sfruttando il nao.

Our team has decided to bring a project on interactive teaching using nao.

Unser Team hat beschlossen, ein Projekt zum interaktiven Unterrichten mit Nao zu starten.

{{< youtube id="mRNOBeh-aJw" >}}
