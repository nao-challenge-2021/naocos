---
title: Pixel Recreation Challenge
date: 2020-12-24
bigimg: [{src: "/img/triangle.jpg"}]
---
{{< gviewer 1YW6lNhNxBAX8Y3gFMBr8ZfXb8aVcr2yb6Tw8oqNHT2U >}}

La squadra ha collaborato per ricreare le tre figure proposte utilizzando i pixel colorati su _Fogli Google_. Il risultato finale è soddisfacente, soprattutto per l'unicorno e il robot,  grazie alla collaborazione dei vari membri della squadra.

The team has worked together for recreating the three figures suggested suing the colored pixels on "Google Sheets". The final result is satisfying, especially for the unicorn and the robot, due to the collaboration between the various group members.

Das Team hat zusammengearbeitet um die drei Figuren die vorgeschlagen wurden mit den färbigen Pixel von "Google Sheets" neu zu erstellen. Das Ergebnis war sehr zufriedenstellend, am meisten was das Einhorn und den Roboter betrifft, dank der Zusammenarbeit von allen Mitgliedern des Teams.
